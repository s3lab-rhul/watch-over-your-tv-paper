# nearby callbacks

com.google.android.gms.nearby.connection.ConnectionLifecycleCallback
com.google.android.gms.nearby.connection.PayloadCallback
com.google.android.gms.nearby.connection.ConnectionResponseCallback
com.google.android.gms.nearby.connection.Connections.ConnectionRequestListener
com.google.android.gms.nearby.connection.Connections.MessageListener
com.google.android.gms.nearby.connection.PayloadCallback$onPayloadReceived


# TV callbacks 

android.media.tv.TvInputService.Session 
android.media.tv.TvInputManager.TvInputCallback 
android.media.tv.TvRecordingClient.RecordingCallback 
Android.media.tv.TvView.TimeShiftPositionCallback 
Android.media.tv.TvView.TvInputCallback 
android.media.tv.TvView$OnUnhandledInputEventListener 
Android.media.session.MediaSessionManager$OnActiveSessionsChangedListener 
android.media.session.MediaController.Callback 
android.media.session.MediaSession.Callback 
android.hardware.input.InputManager.InputDeviceListener 
Androidx.leanback.app.HeadersFragment$OnHeaderClickedListener 
androidx.leanback.app.HeadersFragment$OnHeaderViewSelectedListener 
Androidx.leanback.app.HeadersSupportFragment 
androidx.leanback.app.HeadersSupportFragment$OnHeaderClickedListener 
Androidx.leanback.app.HeadersSupportFragment$OnHeaderViewSelectedListener 
Androidx.leanback.app.SearchFragment.SearchResultProvider 
Androidx.leanback.app.BrowseSupportFragment.BrowseTransitionListener 
Androidx.leanback.app.DetailsFragmentBackgroundController 
Androidx.leanback.app$OnboardingFragment 
androidx.leanback.app$OnboardingSupportFragment 
androidx.leanback.app.PlaybackSupportFragment 
androidx.leanback.app.PlaybackFragment 
androidx.leanback.preference.LeanbackListPreferenceDialogFragment.ViewHolder$OnItemClickListener 
androidx.leanback.media.PlayerAdapter.Callback 
android.media.session.MediaSession.Callback 
android.support.v17.leanback.app.PlaybackOverlayFragment 
Android.widget.VideoView 
Android.media.MediaPlayer 
android.media.MediaSync.Callback 
android.support.v4.media.MediaBrowserCompat.CustomActionCallback 
android.support.v4.media.MediaBrowserCompat.ItemCallback 
androidx.media.VolumeProviderCompat.Callback 
android.support.v4.media.session.MediaControllerCompat.Callback 
android.support.v4.media.session.MediaSessionCompat.Callback 
android.support.v4.media.session.MediaSessionCompat$OnActiveChangeListener 
androidx.media.VolumeProviderCompat.Callback 
androidx.media2.common.SessionPlayer.PlayerCallback 
androidx.media2.session.MediaController.ControllerCallback 
androidx.media2.session.MediaSession.SessionCallback 
androidx.media2.session.RemoteSessionPlayer.Callback 
android.media.tv.TvView 
android.media.tv.TvView.TvInputCallback 
android.media.tv.TvView.TimeShiftPositionCallback 
android.media.tv.TvView.TvInputCallback 
android.media.MediaPlayer 
android.media.MediaPlayer$OnCompletionListener 
android.media.MediaPlayer$OnDrmInfoListener 
android.media.MediaPlayer.TrackInfo 
android.media.MediaPlayer$OnPreparedListener 
android.media.MediaPlayer$OnInfoListener

