The network traces are publicly available in the following link
[link](https://drive.google.com/drive/folders/1jRXv6_HPyGgJBuv5cWxX5xAqKNFYyxuN?usp=sharing)

The network traces can be easilly parsed using haralyzer (pip install haralyzer)
Note that most of the PII were masked in the traces

Usage example:

def get_domains(entries,applog):
    urls = []
    for entry in entries:
        for x in ['request','response']:
            if x in entry.keys() and 'url' in entry[x].keys():
                url = entry[x]['url']
                url = url.replace('http://','')
                url = url.replace('https://','')
                url = url.split('/',1)[0]
                urls.append([applog,url])
    return urls

output_tv = []
har_file = 'app_x_traffic.har'
with open(dir_path + har, 'r') as f:
    har_parser = HarParser(json.loads(f.read()))
    data = har_parser.har_data
    entries = data['entries']
    tmp = get_domains(entries,har_file)
    output_tv.extend(tmp)



The script to instrument the APKs for the dynamic analysis can be found here 
[link](https://github.com/shroudedcode/apk-mitm)
Note that you'll need to provide your MITM certificate using the option --certificate

