# watch-over-your-tv-paper
# Watch over your TV: A Security and Privacy Analysis of the Android TV ecosystem

## Supplementary Material
This repository contains supplementary material for the PETS 2022 paper "Watch over your TV: A Security and Privacy Analysis of the Android TV ecosystem".

Including:
- Dataset:
    - Package names of apps in the mobile streaming category [mob_streaming](artifact/dataset/mob_streaming.txt)
    - Package names of TV apps. Dataset of 4k+ APKs [dataset](artifact/dataset/pkg_names.txt)
- Static analysis:
    - List of TV and media [sources_sinks](artifact/static-analysis/sources_sinks.txt)
    - List of TV and media [callbacks](artifact/static-analysis/callbaks.txt) 
    - Sources [categorization](artifact/static-analysis/sources_cat_v2.csv) and sinks [categorization](static-analysis/sinks_cat_v2.csv)
    - Link to download static dataflow results [External-link](artifact/static-analysis/dataflows.md)
    - APKs [certificate fingerprints](artifact/static-analysis/certificates.csv)
    - List of TPL x APKs [libscout](artifact/static-analysis/libscout_pkgs.csv) and [custom implementation](static-analysis/libraries_manual.csv)
    - Package name of APKs with [malware](artifact/static-analysis/malware_APKs.csv) and [potential (same developers)](static-analysis/malware_potential.csv) 
    - VirusTotal evaluation for APKs with malware [VT-score](artifact/static-analysis/malware_VT.csv)
- Dynamic analysis:
    - Link to download network traces and instrumentation instructions [External-link](artifact/dynamic-analysis/traffic_traces.md)

- Scripts to reproduce results [scripts](artifact/scripts/results.ipynb)
- Instructions to run the notebooks:
- Instructions to run the notebooks:
    1. Clone this repo
    2. Create the docker image. Run the following command in the directory that contains the Dockerfile
        `docker build -t watch-over-your-tv-paper .` 
    3. change the path '/your/path/artifact' in the init_container.sh
    4. run `./init_container.sh`
    5. Go to http://localhost:8888/ 
    6. Run results.ipynb in the folder scripts/
